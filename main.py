from proxy_server import ProxyServer
import urllib3
if __name__ == '__main__':
    proxy = ProxyServer()
    while True:
        proxy.recv_client_request()
        proxy.process_client_request()
    # http = urllib3.PoolManager()
    # response = http.request('GET', 'youtube.com')
    # print(response.headers)