import socket, sys, urllib3, certifi, os, time, datetime
from urllib.request import Request, urlopen, HTTPError
proxy_host = '127.0.0.1'
proxy_port = 10000

response_codes = {
    200: 'OK',
    304: 'Not Modified',
    400: 'Bad Request',
    404: 'Not Found',
    405: 'Method Not Allowed',
    414: 'Request URI too long'
}


class ProxyServer:

    def __init__(self):
        self.server_socket = None
        self.connection = None
        self.request = ""
        self.method = ""
        self.url = ""
        self.host = ""
        self.version = ""
        self.response = ""
        self.rfile = None
        self.wfile = None
        self.server_response = None

        try:
            self.proxy_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.proxy_socket.bind((proxy_host, proxy_port))
        except Exception as e:
            print('Socket creation failure: ', e)
            sys.exit(1)

        # Quantidade de conexões antes de começar a recusar
        self.proxy_socket.listen(30)

    def recv_client_request(self):
        print('Waiting request...')
        self.connection, adderess = self.proxy_socket.accept()
        self.rfile = self.connection.makefile('rb')
        self.wfile = self.connection.makefile('wb')

        request_line = self.rfile.readline()
        while request_line != b'\r\n':
            request_line_decoded = str(request_line.decode('utf-8'))
            request_line_decoded_arr = request_line_decoded.split(' ')
            if len(request_line_decoded_arr) == 3 and request_line_decoded_arr[0] == 'GET':
                self.method = request_line_decoded_arr[0]
                self.url = request_line_decoded_arr[1]
                self.version = request_line_decoded_arr[2].strip('\r\n')
                self.host = self.url.split('/')[2]
                self.request += self.method + ' /' + self.host + ' ' + self.version + '\r\n'
            elif len(request_line_decoded) > 0:
                self.request += request_line_decoded
            request_line = self.rfile.readline()
        self.request += '\r\n'
        print(self.request)

    def get_cache(self):
        filename = self.url.split('//')[1].replace('/', '_').replace('?', ',')
        for file in os.listdir('.\\proxy_cache'):
            if file == filename:
                file_mod_time = os.stat('.\\proxy_cache\\' + filename).st_mtime
                should_time = time.time()
                if file_mod_time - should_time > 86400:
                    print('caiu aqui')
                    os.remove('.\\proxy_cache\\' + filename)
                    break
                else:
                    print('passou')
                    file = open('.\\proxy_cache\\' + filename, 'rb')
                    print(file.read())
                    self.wfile.write(file.read())
                    # self.wfile.close()
                    # self.rfile.close()
                    # self.connection.close()

                    return False
        return True

    def process_client_request(self):
        if self.method != 'GET':
            return

        try:
            self.blacklist_ip()
            self.blacklist_dns()
            self.blacklist_keys()

            # if self.get_cache():

            print('Connecting to web server...')
            self.recv_server_response()
            self.send_client_request()

        except Exception as e:
            print(e)

    def recv_server_response(self):
        if self.method == 'GET':
            http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())
            self.server_response = http.request(self.method, self.url)
            print(self.server_response.headers)

    def change_cache(self):
        filename = self.url.split('//')[1].replace('/', '_').replace('?', ',')
        exists = False
        if self.server_response.getheader('Last-Modified'):
            for file in os.listdir('.\\proxy_cache'):
                if file == filename:
                    exists = True
                    file_modification = time.strptime(time.ctime(os.path.getmtime('.\\proxy_cache\\' + filename)), "%a %b %d %H:%M:%S %Y")
                    request = time.strptime(self.server_response.getheader('Last-Modified'), "%a, %d  %b %Y %H:%M:%S %Z")
                    if file_modification > request:
                        exists = False
                        os.remove('.\\proxy_cache\\' + filename)
                        break
            if not exists:
                response_line = self.version + ' 200 OK\r\n'
                for header in self.server_response.headers:
                    response_line += header + ': ' + self.server_response.getheader(header) + '\r\n'
                response_line += '\r\n'
                file = open('.\\proxy_cache\\' + filename, 'wb')
                cache_response = response_line.encode('UTF-8') + self.server_response.data
                file.write(cache_response)
                file.close()

    def send_client_request(self):
        print(self.server_response.status)
        if self.server_response.status == 200:
            self.change_cache()
            print(self.server_response.data)
            self.wfile.write(self.server_response.data)
        self.wfile.close()
        self.rfile.close()
        self.connection.close()

    def blacklist_dns(self):
        blacklist = open('.\\blacklist_dns.txt', 'r')
        for key in blacklist:
            if key.strip('\n') in self.url:
                forbidden = open('.\\403.png', 'rb')
                self.wfile.write(forbidden.read())
                self.wfile.close()
                print('Blocked content')

    def blacklist_keys(self):
        blacklist = open('.\\blacklist_keys.txt', 'r')
        for key in blacklist:
            if self.url.find(key.strip('\n')) is not -1:
                forbidden = open('.\\403.png', 'rb')
                self.wfile.write(forbidden.read())
                self.wfile.close()
                print('Blocked content')

    def blacklist_ip(self):
        blacklist = open('.\\blacklist_ip.txt', 'r')
        for key in blacklist:
            line = key.split(' ')
            try:
                if proxy_host == line[0]:
                    start_time = line[1].split(':')
                    start_time = datetime.time(hour=int(start_time[0]), minute=int(start_time[1]))
                    end_time = line[2].strip('\n').split(':')
                    end_time = datetime.time(hour=int(end_time[0]), minute=int(end_time[1]))
                    actual_time = datetime.datetime.now().time()
                    if start_time <= actual_time and end_time >= actual_time:
                        forbidden = open('.\\403.png', 'rb')
                        self.wfile.write(forbidden.read())
                        self.wfile.close()
                        print('IP blocked')
            except:
                notfound = open('.\\404.mp4', 'rb')
                self.wfile.write(notfound.read())
                self.wfile.close()
                raise Exception('ERRO 404')

