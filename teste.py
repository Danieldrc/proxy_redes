# import urllib3, certifi
# http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())
# request = http.request('GET', 'https://docs.python.org/3/library/ssl.html')
# print(request.status)
# print(request.headers)
# print(request.data)

# import socket
# import ssl
#
# hostname = 'www.python.org'
# context = ssl.create_default_context()
#
# with socket.create_connection((hostname, 443)) as sock:
#     with context.wrap_socket(sock, server_hostname=hostname) as ssock:
#         print(ssock.)

# import socket
# print(socket.gethostbyname('www.asdasda.gov'))

from http.server import BaseHTTPRequestHandler, HTTPServer
import hashlib
import os
import urllib3


class CacheHandler(http.server.BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(self):
      m = hashlib.md5()
      m.update(self.path)
      cache_filename = m.hexdigest() + ".cached"
      if os.path.exists(cache_filename):
          print("Cache hit")
          data = open(cache_filename).readlines()
      else:
          print("Cache miss")
          data = urllib3.urlopen("http:/" + self.path).readlines()
          open(cache_filename, 'wb').writelines(data)
      self.send_response(200)
      self.end_headers()
      self.wfile.writelines(data)

def run():
    server_address = ('', 8000)
    httpd = BaseHTTPServer.HTTPServer(server_address, CacheHandler)
    httpd.serve_forever()

if __name__ == '__main__':
    run()







